<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package craiglistmarketingpro
 */
global $craiglistmarketingpro_options;
?>

	</div><!-- #content -->
        
            <div class="scrolltoup text-center">
               <div class="scrollup"><i class="fa fa-angle-double-up"></i></div>
            </div>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 text-center">
                                  <p>  <?php  echo$craiglistmarketingpro_options['footer-copyright'] ?> </p>
                        </div>
                        
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 text-center">
                <div class="footer_menu">
                          <?php
                         wp_nav_menu( array(
                             'menu'              => 'footer',
                             'theme_location'    => 'footer',
                             
                             'menu_class'        => 'nav nav-pills',
                             'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                             'walker'            => new wp_bootstrap_navwalker())
                         );
                       ?>

                </div>   
            </div>

                        
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 text-center">


                         <div class="social_link">
                          <ul>
                              <li><a href="<?php echo $craiglistmarketingpro_options['facebook'] ; ?>"><i class="fa fa-facebook"></i></a></li>
                              <li><a href="<?php echo $craiglistmarketingpro_options['twitter'] ; ?>"><i class="fa fa-twitter"></i></li></a>
                              <li><a href="<?php echo $craiglistmarketingpro_options['google_plus'] ; ?>"><i class="fa fa-google-plus"></i></li></a>
                              <li><a href="<?php echo $craiglistmarketingpro_options['linkedin'] ; ?>"><i class="fa fa-linkedin"></i></li></a>
                          </ul>
                        </div>

                        </div>
                    </div>
                </div>

		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

  <!-- ******************************************************************** -->
    <!-- * Custom Footer JavaScript Code ************************************ -->
    <!-- ******************************************************************** -->
    
    <?php if ( (isset($craiglistmarketingpro_options['footer_js'])) && ($craiglistmarketingpro_options['footer_js'] != "") ) : ?>
		<?php echo $craiglistmarketingpro_options['footer_js']; ?>
    <?php endif; ?>

</body>
</html>
