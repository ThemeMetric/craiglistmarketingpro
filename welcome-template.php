<?php 
  /*
  Template Name: Home
  
  */
global $craiglistmarketingpro_options;
get_header(); ?>
<section id="mainslider" class="mainslider">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
     <li data-target="#carousel-example-generic" data-slide-to="3"></li>
  </ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
   <?php query_posts("posts_per_page=1&post_type=carousel"); ?>
   <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>            
         <div class="item active">
        <div class="carosel_img"> 
        <?php the_post_thumbnail('full', array( 'class' => "img-responsive")); ?>
        </div>      
      <div class="carousel-caption">
          <h1 class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".6s"><?php the_title() ; ?></h1>
         <div class="separator-container wow fadeInUp" data-wow-duration=".8s" data-wow-delay=".9s">
          
          </div>
          <div class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".8s"><?php the_content() ; ?></div>
       
        <div class="link">
            <a class="custom-btn wow fadeInUp animated" data-wow-duration=".5s" data-wow-delay=".9s" href="<?php echo $craiglistmarketingpro_options['call_to_action_button_link'] ; ?>" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.9s; animation-name: fadeInUp;"><?php echo $craiglistmarketingpro_options['call_to_action_text'] ; ?></a>
        </div>

      </div>
    </div>
     
   <?php endwhile; ?>
   <?php else : ?>
   <?php endif; wp_reset_query();?>
       
   <?php query_posts("posts_per_page=10&post_type=carousel&offset=1"); ?>
   <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> 
         <div class="item">
        <div class="carosel_img"> 
        <?php the_post_thumbnail('full', array( 'class' => "img-responsive")); ?>
        </div>
        <div class="carousel-caption">
          <h1 class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".6s"><?php the_title() ; ?></h1>
         <div class="separator-container wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".6s">
         
          </div>
          <div class="wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".8s">  <?php the_content() ; ?></div>
        <div class="link">
            <a class="custom-btn wow fadeInUp animated" data-wow-duration=".5s" data-wow-delay=".9s" href="<?php echo $craiglistmarketingpro_options['call_to_action_button_link'] ; ?>" style="visibility: visible; animation-duration: 0.5s; animation-delay: 0.9s; animation-name: fadeInUp;"><?php echo $craiglistmarketingpro_options['call_to_action_text'] ; ?></a>
        </div>

      </div>
    </div>
     
   <?php endwhile; ?>
   <?php else : ?>
   <?php endif; wp_reset_query();?>

    
  </div>
  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <i class="fa fa-angle-left" aria-hidden="true"></i>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
     <i class="fa fa-angle-right" aria-hidden="true"></i>
    <span class="sr-only">Next</span>
  </a>
</div>
    
</section>

<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
                    <div class="container">
                             <?php
			while ( have_posts() ) : the_post();

				the_content();

			endwhile; // End of the loop.
			?>
                           
                    </div>			
		</main><!-- #main -->
	</div><!-- #primary -->
        

<?php get_sidebar( 'content-bottom' ); ?>

<?php get_footer();
