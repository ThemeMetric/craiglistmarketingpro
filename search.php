<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package craiglistmarketingpro
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
                  <div class="container">
                      <div class="row">
                          <div class="col-md-9 col-lg-9 col-sm-9 col-xs-12">
                              
                              <?php
		if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( esc_html__( 'Search Results for : %s', 'craiglistmarketingpro' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				// get_template_part( 'template-parts/content', 'search' );
                        get_template_part( 'template-parts/content', get_post_format() );

			endwhile;

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>
                                    
                <div class="post-pagination text-center"> 
                  <?php the_posts_pagination(array(
                    'next_text' => '<span aria-hidden="true">Next <i class="fa fa-angle-right" aria-hidden="true"></i></span>',
                    'prev_text' => '<span aria-hidden="true"> <i class="fa fa-angle-left" aria-hidden="true"></i> Prev </span>',
                    'screen_reader_text' => ' ',
                    'type'                => 'list'
                    )); ?>
                </div> 
                          </div>
                          <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
                             <?php get_sidebar(); ?>
                              
                          </div>
                      </div>
                  </div>
		
		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_footer();
