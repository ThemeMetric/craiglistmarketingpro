<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package craiglistmarketingpro
 */
global $craiglistmarketingpro_options;
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Craigslist Ad posting service ">

<meta name="msvalidate.01" content="91C963DBC1565DA6C4BB57C2571CDF40" />

<meta name="keywords" content="Craigslist Ad posting,ad poster,cl ad posting,cl posting">

<meta name="author" content="Craiglistmarketingpro">
<link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- ******************************************************************** -->
    <!-- * Custom Favicon *************************************************** -->
    <!-- ******************************************************************** -->

        <link rel="shortcut icon" href="<?php echo $craiglistmarketingpro_options['favicon'][url] ; ?>" /> 
    
      <!-- ******************************************************************** -->
    <!-- * Custom css code ************************************ -->
    <!-- ******************************************************************** -->
    <style type="text/css">
    <?php if ( (isset($craiglistmarketingpro_options['custom-css'])) && ($craiglistmarketingpro_options['custom-css'] != "") ) : ?>
		<?php echo $craiglistmarketingpro_options['custom-css']; ?>
    <?php endif; ?>
    
    </style>
    
      <!-- ******************************************************************** -->
    <!-- * Google analytics Code ************************************ -->
    <!-- ******************************************************************** -->
    <?php wp_head(); ?>
    
    <?php if ( (isset($craiglistmarketingpro_options['analytics_js'])) && ($craiglistmarketingpro_options['analytics_js'] != "") ) : ?>
		<?php echo $craiglistmarketingpro_options['analytics_js']; ?>
    <?php endif; ?>
    

</head>

<body <?php body_class(); ?>>
    
<!-- <div id="loading">
<div id="loading-center">
<div id="loading-center-absolute">
<div class="object" id="object_one"></div>
<div class="object" id="object_two" style="left:20px;"></div>
<div class="object" id="object_three" style="left:40px;"></div>
<div class="object" id="object_four" style="left:60px;"></div>
<div class="object" id="object_five" style="left:80px;"></div>
</div>
</div>
 
</div> -->
    
    
<div id="page" class="site">
	
              
	<header id="masthead" class="site-header" role="banner">
                                    <div class="topbar">
                                         <div class="container">
                                             <div class="row">
                                                 <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                                      <div class="topbar_content pull-right">
                                                         <ul>
                                                             <li><a href="<?php echo $craiglistmarketingpro_options['facebook'] ; ?>"><i class="fa fa-facebook"></i></a></li>
                                                             <li><a href="<?php echo $craiglistmarketingpro_options['twitter'] ; ?>"><i class="fa fa-twitter"></i></li></a>
                                                             <li><a href="<?php echo $craiglistmarketingpro_options['google_plus'] ; ?>"><i class="fa fa-google-plus"></i></li></a>
                                                             <li><a href="<?php echo $craiglistmarketingpro_options['linkedin'] ; ?>"><i class="fa fa-linkedin"></i></li></a>
                                                         </ul>
                                                       </div>

                                                   </div>
                                             </div>
                                         </div>
                                     </div>
            
                                 <nav class="navbar navbar-default  stickynav">
                                   <div class="container">
                                     <!-- Brand and toggle get grouped for better mobile display -->
                                     <div class="navbar-header">
                                       <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                         <span class="sr-only">Toggle navigation</span>
                                         <span class="icon-bar"></span>
                                         <span class="icon-bar"></span>
                                         <span class="icon-bar"></span>
                                       </button>
                                         <div class="logo"> 
                                             <a class="navbar-brand" href="<?php bloginfo('url');  ?>"><img class="img-responsive" src="<?php  echo $craiglistmarketingpro_options['site_logo'][url] ; ?>" alt="Logo" /></a>
                                         </div>
                                      
                                     </div>
                              
                                        <?php
                                        wp_nav_menu( array(
                                            'menu'              => 'primary',
                                            'theme_location'    => 'primary',
                                            'depth'             => 2,
                                            'container'         => 'div',
                                            'container_class'   => 'collapse navbar-collapse',
                                            'container_id'      => 'bs-example-navbar-collapse-1',
                                            'menu_class'        => 'nav navbar-nav navbar-right',
                                            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                            'walker'            => new wp_bootstrap_navwalker())
                                        );
                                      ?>
                                     
                                     
                                   </div><!-- /.container-fluid -->
                                 </nav>

	</header><!-- #masthead -->

	<div id="content" class="site-content">
                                   