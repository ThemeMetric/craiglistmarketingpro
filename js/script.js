
(function($){
    
   "use strict" ;
   
    $(document).ready(function(){
                
    // Sticky Menu
    $(".stickynav").sticky({
        topSpacing: 0
    });
    // carosel 
    $('.carousel').carousel({
  interval: 5000
});
$(".carousel-inner .item:first-child").addClass("active");
// panel
 $(".panel-heading").addClass("collapsed");
 
 //Tab 

  $('#tabs').tab();
// wow js
var wow = new WOW(
  {
    boxClass:     'wow',      // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset:       0,          // distance to the element when triggering the animation (default is 0)
    mobile:       true,       // trigger animations on mobile devices (default is true)
    live:         true,       // act on asynchronously loaded content (default is true)
    callback:     function(box) {
      // the callback is fired every time an animation is started
      // the argument that is passed in is the DOM node being animated
    },
    scrollContainer: null // optional scroll container selector, otherwise use window
  }
);
wow.init();

 
    // click to scroll up

        $('.scrollup').click(function(){
                $("html,body").animate({
                        scrollTop:0
                },600);
                return false;
        });
                $(window).on('scroll',function(){

                        if($(this).scrollTop() >250){
                                $('.scrollup').fadeIn();
                        }else{
                                $('.scrollup').fadeOut();
                        }

                });

                 
     
                       
        // jQuery Custom scrollbar
		$("body").niceScroll({
			cursorcolor: "#14A3F9",
			cursorborderradius: "0px",
			cursorwidth: "10px",
			cursorminheight: 100,
			cursorborder: "0px solid #14A3F9",
			zindex: 9999,
			autohidemode: false,
			horizrailenabled:false
		});
          
         
    });
    
    // preloader
    $(window).load(function() {
	$("#loading").delay(500).fadeOut(500);
	$("#loading-center").click(function() {
	$("#loading").fadeOut(500);
	});
});
})(jQuery);
