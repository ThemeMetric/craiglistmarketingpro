<?php
/**
 * The template for the content bottom widget areas on posts and pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

if ( ! is_active_sidebar( 'footer-1' ) && ! is_active_sidebar( 'footer-2' ) && ! is_active_sidebar( 'footer-3' ) && ! is_active_sidebar( 'footer-4' ) ) {
	return;
}

// If we get this far, we have widgets. Let's do this.
?>

<aside id="content-bottom-widgets" class="content-bottom-widgets" role="complementary">
    
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                <?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
		<div class="widget-area">
			<?php dynamic_sidebar( 'footer-1' ); ?>
		</div><!-- .widget-area -->
	<?php endif; ?>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                	<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
		<div class="widget-area">
			<?php dynamic_sidebar( 'footer-2' ); ?>
		</div><!-- .widget-area -->
	<?php endif; ?>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                   
                <?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
		<div class="widget-area">
			<?php dynamic_sidebar( 'footer-3' ); ?>
		</div><!-- .widget-area -->
	<?php endif; ?>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                 <?php if ( is_active_sidebar( 'footer-4' ) ) : ?>
		<div class="widget-area">
			<?php dynamic_sidebar( 'footer-4' ); ?>
		</div><!-- .widget-area -->
	<?php endif; ?>
            </div>
        </div>
    </div>
    	
</aside><!-- .content-bottom-widgets -->
