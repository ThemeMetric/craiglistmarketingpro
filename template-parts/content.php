<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package craiglistmarketingpro
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    
	<div class="entry-content">
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
                <div class="post-media"> 
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('blog-isotope'); ?></a>

               </div>
         
                </div>
            <div class="col-md-9 col-lg-9 col-sm-9 col-xs-12">
                <div class="post-info">
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
             
                </div>
             <div class="post-body">
              <p><?php echo wp_trim_words( get_the_content(), 30, '' ); ?></p>
             
            </div>
            
            <footer class="entry-footer">
                <div class="author_bar">
                    
                    <h6 class="upper"> <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4"> <?php the_tags( '', ', ', '' ); ?></div> <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4"><a href="<?php echo site_url(); ?>/author/<?php the_author(); ?>"> <?php the_author(); ?></a></div> <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4"><span><?php the_time('d F Y'); ?></span> </div></h6>
                </div>
	     </footer><!-- .entry-footer -->
          
	  </div>
             
	</div><!-- .entry-content -->

</article><!-- #post-## -->
