<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package craiglistmarketingpro
 */

?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">
   
             <div class="post-body">
       <?php the_content(); ?>
             
            </div>
    
	</div><!-- .entry-content -->
</article><!-- #post-## -->
