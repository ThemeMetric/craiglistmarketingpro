<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



// About one section shortcode


add_shortcode('about-one', function($attr, $content){
	ob_start(); 

extract( shortcode_atts(array(
	'title' => '',	          
	'description' => '',
        'image' => '',

), $attr) );

?>

	 <section class="about_page_top">
            <div class="container">
                <div class="row"> 
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 wow fadeInLeft">
                        <div class="about_page_content"> 
                            <h3><?php echo $title ; ?></h3>
                            
                            <p><?php echo $description ; ?></p>
                        
                        </div>
                        
                    </div>
                   <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 wow fadeInRight">
                       <div class="about_page_content"> 
                   <?php $about_photo1 = wp_get_attachment_image_src($image, 'full') ; ?>
                        <img class="img-responsive center-block" src="<?php echo $about_photo1[0]; ?>" alt="about" />
                        
                        </div>
                       
                   </div>
                
                </div>
            </div>
        </section>

	<?php return ob_get_clean();
});

// Contact page title desc address shortcode


add_shortcode('contact-address', function($attr, $content){
	ob_start(); 

extract( shortcode_atts(array(
	'title' => '',	          
	'description' => '',
        'address' => '',
        'phone' => '',
        'email' => '',

), $attr) );

?>

	  <section class="get_in_touch text-center">
            <div class="container">
                <div class="row">
                    <div class="contact_page_title wow bounceInUp"> 
                       <h2><?php echo $title ; ?></h2>              
                    </div>
                     <div class="contact_desc wow fadeIn"> 
                       <p><?php echo $description ; ?></p>
                     </div>
                    
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 wow rollIn">
                        <div class="contact_address"> 
                          <i class="fa fa-map-marker"></i>
                          <p><?php echo $address ; ?></p>
                        </div>
                        
                    </div>
                      <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <div class="contact_address border_right_left"> 
                          <i class="fa fa-phone"></i>
                          <p><?php echo $phone ; ?></p>
                         
                        </div>
                        
                    </div>
                      <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 wow lightSpeedIn">
                        <div class="contact_address"> 
                          <i class="fa fa-envelope"></i>
                          <p><?php echo $email ; ?></p>
                         
                        </div>
                        
                    </div>
                   
                    
                    
                </div>
            </div>
            
        </section>

	<?php return ob_get_clean();
});

// Contact form heading


add_shortcode('contact_form_heading', function($attr, $content){
	ob_start(); 

extract( shortcode_atts(array(
	'title' => '',	          
	'description' => '',
       

), $attr) );

?>
 <div class="contact_title text-center wow fadeInDown"> 
          <h2><?php echo $title ; ?></h2>
          <p><?php echo $description ; ?></p>
        
        </div>
	 
	<?php return ob_get_clean();
});


// About home first elements


add_shortcode('about_home', function($attr, $content){
	ob_start(); 

extract( shortcode_atts(array(
	'title' => '',	          
	'description' => '',
	'button_title' => '',
	'button_link' => '',
       

), $attr) );

?>
 
<section class="about">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12"> 
                <div class="section_title text-center">                  
                   <h2><?php echo $title ; ?></h2>  
                </div>
                <div class="section_description"> 
        <p><?php echo $description ; ?></p>
                </div>
                
                <div class="about_more_link  text-center">
  <a class="btn btn-primary"href="<?php echo $button_link ; ?>"><?php echo $button_title ; ?></a>
                </div>
                
            </div>
        </div>
    </div>
</section>
	 
	<?php return ob_get_clean();
});


// Home we Acept short code


add_shortcode('we_accept_home_block', function($attr, $content){
	ob_start(); 

extract( shortcode_atts(array(
	'title' => '',	          
	'image' => '',
       

), $attr) );

?>
<section class="Weacept">   
    <div class="weacpt_title text-center">
        <h1><?php echo $title ; ?></h1>       
    </div>
    <?php $weaccept_photo1 = wp_get_attachment_image_src($image, 'full') ; ?>
  <img class="img-responsive"src="<?php echo $weaccept_photo1[0]; ?>" alt="" />
</section>
	 
	<?php return ob_get_clean();
});


// Home who we are home block


add_shortcode('who_we_are_home_block', function($attr, $content){
	ob_start(); 

extract( shortcode_atts(array(
	'title' => '',	          	 
	          
	'left_desc' => '',	          
	'left_icon' => 'fa fa-certificate',	          
	'right_icon' => 'fa fa-star',	          
	'right_desc' => '',	          
	
       

), $attr) );

?>



<section class="whoweare">
    <div class="container">
        <div class="row">
            <div class="whoweare_title text-center">
                <h2><?php echo $title ; ?></h2>
            </div>
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 wow fadeIn">
                
                   <div class="whoweare_icon text-center">
                     <i class="<?php echo $left_icon ;?>" aria-hidden="true"></i>
                </div>
           <p><?php echo $left_desc ; ?></p>

              
            
            </div>
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 wow fadeIn">
                <div class="whoweare_icon text-center">
                     <i class="<?php echo $right_icon ; ?>" aria-hidden="true"></i>
                </div>
                
            <p><?php echo $right_desc ; ?></p>
            </div>
        </div>
     
    </div>
</section>
	 
	<?php return ob_get_clean();
});

// Home What we do block short code


add_shortcode('what_we_do_home_block', function($attr, $content){
	ob_start(); 
extract( shortcode_atts(array(
	'title' => '',	          	 
	'block_one_title' => '',	          	 
	'block_one_desc' => '',	
        'block_two_title' => '',	          	 
	'block_two_desc' => '',	
        'block_three_title' => '',	          	 
	'block_three_desc' => '',		 
), $attr) );
?>
<section class="whoweare">
    <div class="container">            
        <div class="row">
            <div class="whoweare_title text-center">
                <h2><?php echo $title ;?></h2>
            </div>          
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 wow fadeIn">
                <h3 class="text-center"><?php echo $block_one_title ;?></h3>
                 <p><?php echo $block_one_desc ;?> </p>              
            </div>
             <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 wow fadeInUp">
                 <h3 class="text-center"><?php echo $block_two_title ;?></h3>
               <p><?php echo $block_two_desc ;?></p>          
            </div>
              <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 wow fadeIn">
                <h3 class="text-center"><?php echo $block_three_title ;?></h3>
                <p><?php echo $block_three_desc ;?></p>              
            </div>      
        </div>
    </div>
</section>
	 
	<?php return ob_get_clean();
});


// Pricing Table Shortcode


add_shortcode('pricing_table', function($attr, $content){
	ob_start(); 

extract( shortcode_atts(array(
	'table_one_title' => '',
    
	'table_one_desc1' => '',
        'table_one_price1' => '',
        'table_one_button_text1' => '',	          
	'table_one_button_link1' => '',
    
	'table_one_desc2' => '',	          
	'table_one_price2' => '',	          
	'table_one_button_text2' => '',	          
	'table_one_button_link2' => '',
    
    
        'table_two_title' => '',
    
        'table_two_desc1' => '',
        'table_two_price1' => '',
        'table_two_button_text1' => '',	          
	'table_two_button_link1' => '',
    
        'table_two_desc2' => '',
        'table_two_price2' => '',
        'table_two_button_text2' => '',	          
	'table_two_button_link2' => '',
    
        'table_two_desc3' => '',
        'table_two_price3' => '',
        'table_two_button_text3' => '',	          
	'table_two_button_link3' => '',
    
	
        'table_three_title' => '',
    
        'table_three_desc1' => '',
        'table_three_price1' => '',
        'table_three_button_text1' => '',	          
	'table_three_button_link1' => '',
    
        'table_three_desc2' => '',
        'table_three_price2' => '',
        'table_three_button_text2' => '',	          
	'table_three_button_link2' => '',
    
        'table_three_desc3' => '',
        'table_three_price3' => '',
        'table_three_button_text3' => '',	          
	'table_three_button_link3' => '',
    	 
	
), $attr) );

?>
<section class="pricing_table text-center">
            <div class="container">
                <div class="row"> 
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <div class="pricing_table_single"> 
                            <div class="pricing_head"> 
                               <h2><?php echo $table_one_title ; ?></h2>
                            </div>
                            <div class="pricing_body"> 
                              <?php echo $table_one_desc1 ; ?>
                               
                            </div>
                           
                            <div class="pricing_footer"> 
                              <p><?php echo $table_one_price1 ; ?></p>
      <a class="btn btn-primary" href="<?php echo $table_one_button_link1 ; ?>"><?php echo $table_one_button_text1 ; ?></a>
                            </div>
                              <div class="pricing_body"> 
                              <?php echo $table_one_desc2 ; ?>
                                
                            </div>
                           
                            <div class="pricing_footer"> 
                              <p><?php echo $table_one_price2 ; ?></p>
      <a class="btn btn-primary" href="<?php echo $table_one_button_link2 ; ?>"><?php echo $table_one_button_text2 ; ?></a>
                            </div>
                        </div>
                    </div> 
                    
                       <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <div class="pricing_table_single"> 
                            <div class="pricing_head"> 
                               <h2><?php echo $table_two_title ; ?></h2>
                            </div>
                            <div class="pricing_body"> 
                              <?php echo $table_two_desc1 ; ?>
                            </div>
                            <div class="pricing_footer"> 
                              <p><?php echo $table_two_price1 ; ?></p>
      <a class="btn btn-primary" href="<?php echo $table_two_button_link1 ; ?>"><?php echo $table_two_button_text1 ; ?></a>
                            </div>
                            
                             <div class="pricing_body"> 
                              <?php echo $table_two_desc2 ; ?>
                            </div>
                            <div class="pricing_footer"> 
                              <p><?php echo $table_two_price2 ; ?></p>
      <a class="btn btn-primary" href="<?php echo $table_two_button_link2 ; ?>"><?php echo $table_two_button_text2 ; ?></a>
                            </div>
                            
                             <div class="pricing_body"> 
                              <?php echo $table_two_desc3 ; ?>
                            </div>
                            <div class="pricing_footer"> 
                              <p><?php echo $table_two_price3 ; ?></p>
      <a class="btn btn-primary" href="<?php echo $table_two_button_link3 ; ?>"><?php echo $table_two_button_text3 ; ?></a>
                            </div>
                            
                        </div>
                    </div>  
                    
                 
                    
                     <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <div class="pricing_table_single"> 
                            <div class="pricing_head"> 
                               <h2><?php echo $table_three_title ; ?></h2>
                            </div>
                            <div class="pricing_body"> 
                              <?php echo $table_three_desc1 ; ?>
                            </div>
                            <div class="pricing_footer"> 
                              <p><?php echo $table_three_price1 ; ?></p>
      <a class="btn btn-primary" href="<?php echo $table_three_button_link1 ; ?>"><?php echo $table_three_button_text1 ; ?></a>
                            </div>
                            
                             <div class="pricing_body"> 
                              <?php echo $table_three_desc2 ; ?>
                            </div>
                            <div class="pricing_footer"> 
                              <p><?php echo $table_three_price2 ; ?></p>
      <a class="btn btn-primary" href="<?php echo $table_three_button_link2 ; ?>"><?php echo $table_three_button_text2 ; ?></a>
                            </div>
                            
                            
                             <div class="pricing_body"> 
                              <?php echo $table_three_desc3 ; ?>
                            </div>
                            <div class="pricing_footer"> 
                              <p><?php echo $table_three_price3 ; ?></p>
      <a class="btn btn-primary" href="<?php echo $table_three_button_link3 ; ?>"><?php echo $table_three_button_text3 ; ?></a>
                            </div>
                            
                        </div>
                    </div> 
                
                </div>
            </div>
        </section>
	 
	<?php return ob_get_clean();
});


// Testimonial shortcode
add_shortcode('testimonial_home', function($attr, $content){
	ob_start(); 
extract( shortcode_atts(array(
	'title' => '',	          	 
			 
), $attr) );
?>
      
<div class="testimonial-area overliybg text-center">
<div class="container">
        <div class="row">
                <div class="col-md-12">
                        <div class="title text-center">
                                <h1><?php echo $title ; ?></h1>
                               
                        </div>
<div id="testCarousel" class="carousel slide" data-ride="carousel">

<!-- Wrapper for slides -->
<div class="carousel-inner" role="listbox">

        <?php
        $latest = new WP_Query(array(
                'post_type' => 'testimonials',
                'posts_per_page' => 10,
                'orderby' =>'menu_order',
                'order' => 'asc'
        ));
?>

<?php while($latest->have_posts()) : $latest->the_post(); ?>
 
        <div class="item">
                <div class="testimonial-list">
                        <div class="single-testimonial">
                                <div class="qutations">
                                        <?php the_content(); ?>
                                </div>
                        <div class="managing-img img-circle">
              <?php the_post_thumbnail(); ?>
                               
                               <h4><?php the_title(); ?></h4> 
                               
                        </div>
                        
                        </div>
                </div>
        </div>

<?php endwhile; ?>
</div>

<!-- Left and right controls -->
      <a class="left ktitslider" href="#testCarousel" role="button" data-slide="prev">
              <i class="fa fa-angle-left"></i>
      </a>
        <a class="right ktitslider" href="#testCarousel" role="button" data-slide="next">
              <i class="fa fa-angle-right"></i>
        </a>

</div>	

              </div>
      </div>
</div>
</div>
	 
	<?php return ob_get_clean();
});
