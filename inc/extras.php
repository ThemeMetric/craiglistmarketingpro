<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package craiglistmarketingpro
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function craiglistmarketingpro_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'craiglistmarketingpro_body_classes' );



function craiglistmarketingpro_fonts_url(){


	$fonts = array(
		'Roboto:400,300,500,700'
		
	);


	$fonts_url = add_query_arg(array(
		'family' => urlencode( implode('|', $fonts) ),
		'subset' => urlencode( 'latin' )
	), 'https://fonts.googleapis.com/css');

	return $fonts_url;

}



/*===================================================================================
 * Search Form
 * =================================================================================*/
add_filter('get_search_form', 'craiglistmarketingpro_search_form');

function craiglistmarketingpro_search_form($form) {
	$form = '<form role="search" class="search-form" method="get" action="' . esc_url( home_url( '/' ) ) . '" >
		<input class="search-field" type="text" name="s" id="s" value="' . esc_attr( get_search_query() ) . '" placeholder="Search" required>
		 <button type="submit" id="search-submit" class="search-submit"><i class="fa fa-search"></i></button>
	</form>';

return $form;
}


// Carousel Custom Post
add_action( 'init', 'carousel_custom_post' );
function carousel_custom_post() {

  register_post_type( 'carousel',
    array(
      'labels' => array(
        'name' => __( 'Slider' ),
        'singular_name' => __( 'Slider' ),
        'add_new_item' => __( 'Add New Slider ' )
      ),
    'public' => true,
	'supports' => array('title', 'editor', 'thumbnail'),
    'has_archive' => true,
    )
  );
  	//testimonials 
	register_post_type ('testimonials', array(
		'labels' => array(
			'name' => 'Testimonials',
			'add_new_item' => 'add new our testimonials'
		),
		'public' => true,
		'supports' => array('title', 'editor', 'thumbnail', 'page-attributes')
	));

  
}



if(function_exists('vc_map')){


	// About page upper block

	vc_map(array(
		'name' => __('About One','craiglistmarketingpro'),
		'base' => 'about-one',
            'category' => __( 'Craiglistmarketingpro', 'craiglistmarketingpro' ),
     
             'icon' => 'icon-wpb-wp',
		'params' => array(
			array(
				'type' => 'textfield',
				'param_name' => 'title',
				'value' => '',
				'heading' => 'Title'
			),
			
			array(
				'type' => 'textarea_html',
				'param_name' => 'description',
				'value' => '',
				'heading' => 'Content'
			),
			
		
			array(
				'type' => 'attach_image',
				'param_name' => 'image',
				'value' => '',
				'heading' => 'Section one image'
			),
			
			


		)
	));
	
        // Contact 

	vc_map(array(
		'name' => __('Contact top','craiglistmarketingpro'),
		'base' => 'contact-address',
            'category' => __( 'Craiglistmarketingpro', 'craiglistmarketingpro' ),
     
             'icon' => 'icon-wpb-wp',
		'params' => array(
			array(
				'type' => 'textfield',
				'param_name' => 'title',
				'value' => '',
				'heading' => 'Title'
			),
			
			array(
				'type' => 'textarea',
				'param_name' => 'description',
				'value' => '',
				'heading' => 'Contact description'
			),
			
                    array(
				'type' => 'textarea',
				'param_name' => 'address',
				'value' => '',
				'heading' => 'Contact Address'
			),
                    array(
				'type' => 'textarea',
				'param_name' => 'phone',
				'value' => '',
				'heading' => 'Contact Phone Numbers'
			),
                    array(
				'type' => 'textarea',
				'param_name' => 'email',
				'value' => '',
				'heading' => 'Contact email'
			),
		
			
			


		)
	));
            // Contact form heading 

	vc_map(array(
		'name' => __('Contact form Heading','craiglistmarketingpro'),
		'base' => 'contact_form_heading',
            'category' => __( 'Craiglistmarketingpro', 'craiglistmarketingpro' ),
     
             'icon' => 'icon-wpb-wp',
		'params' => array(
			array(
				'type' => 'textfield',
				'param_name' => 'title',
				'value' => '',
				'heading' => 'Title'
			),
			
			array(
				'type' => 'textarea',
				'param_name' => 'description',
				'value' => '',
				'heading' => 'Contact description'
			),
			
                   
		)
	));
        // About home section

	vc_map(array(
		'name' => __('About home Section block','craiglistmarketingpro'),
		'base' => 'about_home',
            'category' => __( 'Craiglistmarketingpro', 'craiglistmarketingpro' ),
     
             'icon' => 'icon-wpb-wp',
		'params' => array(
			array(
				'type' => 'textfield',
				'param_name' => 'title',
				'value' => '',
				'heading' => 'Title'
			),
			
			array(
				'type' => 'textarea_html',
				'param_name' => 'description',
				'value' => '',
				'heading' => 'Contact description'
			),
                    array(
				'type' => 'textfield',
				'param_name' => 'button_title',
				'value' => '',
				'heading' => 'Home about button title.'
			),
                    array(
				'type' => 'textfield',
				'param_name' => 'button_link',
				'value' => '',
				'heading' => 'Home about button link.'
			),
			
                   
		)
	));
        
          // We accept home block

	vc_map(array(
		'name' => __('We accept home block','craiglistmarketingpro'),
		'base' => 'we_accept_home_block',
            'category' => __( 'Craiglistmarketingpro', 'craiglistmarketingpro' ),
     
             'icon' => 'icon-wpb-wp',
		'params' => array(
			array(
				'type' => 'textfield',
				'param_name' => 'title',
				'value' => '',
				'heading' => 'Title'
			),
			array(
				'type' => 'attach_image',
				'param_name' => 'image',
				'value' => '',
				'heading' => 'We accept home block image'
			),
		
			
                   
		)
	));
         // Who we are home block

	vc_map(array(
		'name' => __('Who we are home block','craiglistmarketingpro'),
		'base' => 'who_we_are_home_block',
            'category' => __( 'Craiglistmarketingpro', 'craiglistmarketingpro' ),
     
             'icon' => 'icon-wpb-wp',
		'params' => array(
			array(
				'type' => 'textfield',
				'param_name' => 'title',
				'value' => '',
				'heading' => 'Title'
			),
              
                    
                    array(
				'type' => 'dropdown',
				'heading' => __( 'Icon library', 'craiglistmarketingpro' ),
				'value' => array(
					__( 'Font Awesome', 'craiglistmarketingpro' ) => 'fontawesome',
					__( 'Open Iconic', 'craiglistmarketingpro' ) => 'openiconic',
					__( 'Typicons', 'craiglistmarketingpro' ) => 'typicons',
					__( 'Entypo', 'craiglistmarketingpro' ) => 'entypo',
					__( 'Linecons', 'craiglistmarketingpro' ) => 'linecons',
					__( 'Mono Social', 'craiglistmarketingpro' ) => 'monosocial',
				),
				'admin_label' => true,
				'param_name' => 'type',
				'description' => __( 'Select icon library.', 'js_composer' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => __( 'Left Block Icon', 'craiglistmarketingpro' ),
				'param_name' => 'left_icon',
				'value' => 'fa fa-adjust', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false,
					// default true, display an "EMPTY" icon?
					'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
				),
                    
                    ),
                    
              
                      array(
				'type' => 'textarea',
				'param_name' => 'left_desc',
				'value' => '',
				'heading' => 'Left block Description'
			),
		
                    
                    array(
				'type' => 'dropdown',
				'heading' => __( 'Icon library', 'craiglistmarketingpro' ),
				'value' => array(
					__( 'Font Awesome', 'craiglistmarketingpro' ) => 'fontawesome',
					__( 'Open Iconic', 'craiglistmarketingpro' ) => 'openiconic',
					__( 'Typicons', 'craiglistmarketingpro' ) => 'typicons',
					__( 'Entypo', 'craiglistmarketingpro' ) => 'entypo',
					__( 'Linecons', 'craiglistmarketingpro' ) => 'linecons',
					__( 'Mono Social', 'craiglistmarketingpro' ) => 'monosocial',
				),
				'admin_label' => true,
				'param_name' => 'type',
				'description' => __( 'Select icon library.', 'js_composer' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => __( 'Right Block Icon', 'craiglistmarketingpro' ),
				'param_name' => 'right_icon',
				'value' => 'fa fa-star', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false,
					// default true, display an "EMPTY" icon?
					'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
				),
                    
                    ),
                  
		       array(
				'type' => 'textarea',
				'param_name' => 'right_desc',
				'value' => '',
				'heading' => 'Right block Description'
			),       
                    
                   
		)
	));
        
             // We accept home block

	vc_map(array(
		'name' => __('What we do home block','craiglistmarketingpro'),
		'base' => 'what_we_do_home_block',
                'category' => __( 'Craiglistmarketingpro', 'craiglistmarketingpro' ),
                'icon' => 'icon-wpb-wp',
		'params' => array(
			array(
				'type' => 'textfield',
				'param_name' => 'title',
				'value' => '',
				'heading' => 'Section Title'
			),
                    	
                    array(
				'type' => 'textfield',
				'param_name' => 'block_one_title',
				'value' => '',
				'heading' => 'Block one title'
			),
                      array(
				'type' => 'textarea',
				'param_name' => 'block_one_desc',
				'value' => '',
				'heading' => 'Block one description'
			),
                    array(
				'type' => 'textfield',
				'param_name' => 'block_two_title',
				'value' => '',
				'heading' => 'Block Two title'
			),
                     array(
				'type' => 'textarea',
				'param_name' => 'block_two_desc',
				'value' => '',
				'heading' => 'Block Two description'
			),
			 array(
				'type' => 'textfield',
				'param_name' => 'block_three_title',
				'value' => '',
				'heading' => 'Block Three title'
			),
                     array(
				'type' => 'textarea',
				'param_name' => 'block_three_desc',
				'value' => '',
				'heading' => 'Block Three description'
			),
		
			
                   
		)
	));
        
         
        // Pricing table

	vc_map(array(
		'name' => __('Pricing Table','craiglistmarketingpro'),
		'base' => 'pricing_table',
            'category' => __( 'Craiglistmarketingpro', 'craiglistmarketingpro' ),
     
             'icon' => 'icon-wpb-wp',
		'params' => array(
			array(
				'type' => 'textfield',
				'param_name' => 'table_one_title',
				'value' => '',
				'heading' => 'Table One Title'
			),
                    array(
				'type' => 'textarea',
				'param_name' => 'table_one_desc1',
				'value' => '',
				'heading' => 'Table One First Description'
			),
			
                    array(
				'type' => 'textfield',
				'param_name' => 'table_one_price1',
				'value' => '',
				'heading' => 'Table One first Price'
			),
			array(
				'type' => 'textfield',
				'param_name' => 'table_one_button_text1',
				'value' => '',
				'heading' => 'Table One first Button Text'
			),
			array(
				'type' => 'textfield',
				'param_name' => 'table_one_button_link1',
				'value' => '',
				'heading' => 'Table One first Button Link'
			),
                         array(
				'type' => 'textarea',
				'param_name' => 'table_one_desc2',
				'value' => '',
				'heading' => 'Table One 2nd Description'
			),
			
                    array(
				'type' => 'textfield',
				'param_name' => 'table_one_price2',
				'value' => '',
				'heading' => 'Table One 2nd Price'
			),
			array(
				'type' => 'textfield',
				'param_name' => 'table_one_button_text2',
				'value' => '',
				'heading' => 'Table One 2nd Button Text'
			),
			array(
				'type' => 'textfield',
				'param_name' => 'table_one_button_link2',
				'value' => '',
				'heading' => 'Table One 2nd Button Link'
			),
			// table one end 
                    
                    array(
				'type' => 'textfield',
				'param_name' => 'table_two_title',
				'value' => '',
				'heading' => 'Table Two Title'
			),
                    array(
				'type' => 'textarea',
				'param_name' => 'table_two_desc1',
				'value' => '',
				'heading' => 'Table Two 1 Description'
			),
			
                    array(
				'type' => 'textfield',
				'param_name' => 'table_two_price1',
				'value' => '',
				'heading' => 'Table Two 1 Price'
			),
			array(
				'type' => 'textfield',
				'param_name' => 'table_two_button_text1',
				'value' => '',
				'heading' => 'Table Two 1 Button Text'
			),
			array(
				'type' => 'textfield',
				'param_name' => 'table_two_button_link1',
				'value' => '',
				'heading' => 'Table Two 1 Button Link'
			),
                    
                    
                            array(
				'type' => 'textarea',
				'param_name' => 'table_two_desc2',
				'value' => '',
				'heading' => 'Table Two 2 Description'
			),
			
                    array(
				'type' => 'textfield',
				'param_name' => 'table_two_price2',
				'value' => '',
				'heading' => 'Table Two 2 Price'
			),
			array(
				'type' => 'textfield',
				'param_name' => 'table_two_button_text2',
				'value' => '',
				'heading' => 'Table Two 2 Button Text'
			),
			array(
				'type' => 'textfield',
				'param_name' => 'table_two_button_link2',
				'value' => '',
				'heading' => 'Table Two 2 Button Link'
			),
                    
                    
                    
                            array(
				'type' => 'textarea',
				'param_name' => 'table_two_desc3',
				'value' => '',
				'heading' => 'Table Two 3 Description'
			),
			
                    array(
				'type' => 'textfield',
				'param_name' => 'table_two_price3',
				'value' => '',
				'heading' => 'Table Two 3 Price'
			),
			array(
				'type' => 'textfield',
				'param_name' => 'table_two_button_text3',
				'value' => '',
				'heading' => 'Table Two 3 Button Text'
			),
			array(
				'type' => 'textfield',
				'param_name' => 'table_two_button_link3',
				'value' => '',
				'heading' => 'Table Two 3 Button Link'
			),
                    
                    
                    // table two end
                    
			array(
				'type' => 'textfield',
				'param_name' => 'table_three_title',
				'value' => '',
				'heading' => 'Table Three Title'
			),
                    
                    array(
				'type' => 'textarea',
				'param_name' => 'table_three_desc1',
				'value' => '',
				'heading' => 'Table Three 1 Description'
			),
			
                    array(
				'type' => 'textfield',
				'param_name' => 'table_three_price1',
				'value' => '',
				'heading' => 'Table Three 1 Price'
			),
			array(
				'type' => 'textfield',
				'param_name' => 'table_three_button_text1',
				'value' => '',
				'heading' => 'Table Three 1 Button Text'
			),
			array(
				'type' => 'textfield',
				'param_name' => 'table_three_button_link1',
				'value' => '',
				'heading' => 'Table Three  1 Button Link'
			),
	
                                        array(
				'type' => 'textarea',
				'param_name' => 'table_three_desc2',
				'value' => '',
				'heading' => 'Table Three 2 Description'
			),
			
                    array(
				'type' => 'textfield',
				'param_name' => 'table_three_price2',
				'value' => '',
				'heading' => 'Table Three 2 Price'
			),
			array(
				'type' => 'textfield',
				'param_name' => 'table_three_button_text2',
				'value' => '',
				'heading' => 'Table Three 2 Button Text'
			),
			array(
				'type' => 'textfield',
				'param_name' => 'table_three_button_link2',
				'value' => '',
				'heading' => 'Table Three  2 Button Link'
			),
                  
                                        array(
				'type' => 'textarea',
				'param_name' => 'table_three_desc3',
				'value' => '',
				'heading' => 'Table Three 3 Description'
			),
			
                    array(
				'type' => 'textfield',
				'param_name' => 'table_three_price3',
				'value' => '',
				'heading' => 'Table Three 3 Price'
			),
			array(
				'type' => 'textfield',
				'param_name' => 'table_three_button_text3',
				'value' => '',
				'heading' => 'Table Three 3 Button Text'
			),
			array(
				'type' => 'textfield',
				'param_name' => 'table_three_button_link3',
				'value' => '',
				'heading' => 'Table Three  3 Button Link'
			),
			
                   
		)
	));
        
        
          // Testi monial

	vc_map(array(
		'name' => __('Testimonial','craiglistmarketingpro'),
		'base' => 'testimonial_home',
            'category' => __( 'Craiglistmarketingpro', 'craiglistmarketingpro' ),
     
             'icon' => 'icon-wpb-wp',
		'params' => array(
			array(
				'type' => 'textfield',
				'param_name' => 'title',
				'value' => '',
				'heading' => 'Testi monial Section Title'
			),
			
		)
	));
        
        
}

// Replace WordPress login logo with your own
add_action('login_head', 'sms_custom_login_logo');
function sms_custom_login_logo() {
    echo '<style>
    h1 a { background-image:url("http://craiglistmarketingpro.com/wp-content/uploads/2016/05/fav.png") !important;  margin-bottom: 0 !important; padding-bottom: 0 !important; }
    .login form { margin-top: 10px !important; }
    </style>';
}


// Change the URL of the WordPress login logo
add_filter('login_headerurl', 'sms_url_login_logo');
function sms_url_login_logo(){
    return get_bloginfo( 'home' ); // Put your logo URL here
}


//* Change login logo title
add_filter( 'login_headertitle', 'sms_login_logo_url_title' );
function sms_login_logo_url_title() {
  return 'Craiglistmarketingpro'; // Put your logo URL title here
}
// Change the welcome message of admin bar
add_filter('gettext', 'sms_change_howdy', 10, 3);

function sms_change_howdy($translated, $text, $domain) {
 
    if (!is_admin() || 'default' != $domain)
        return $translated;
 
    if (false !== strpos($translated, 'Howdy'))
        return str_replace('Howdy', 'কেমন আছেন', $translated);
 
    return $translated;
}


// Change Dashboard header logo
add_action('admin_head', 'sms_custom_logo');
function sms_custom_logo() {
echo '
        <style type="text/css">
        #wp-admin-bar-wp-logo > .ab-item .ab-icon:before { 
	background-image: url("http://s6.postimg.org/85ubj8zgx/fav.png") !important; 
	background-position: 0 0;
        color:rgba(0, 0, 0, 0);
	}
        #wpadminbar #wp-admin-bar-wp-logo.hover > .ab-item .ab-icon {
	background-position: 0 0;
	}	
</style>
';
}
// Change footer message
add_filter('admin_footer_text', 'change_footer_message');
function change_footer_message () { 
       echo 'Powered By Craiglistmarketingpro'; 
}
// Remove footer version
add_action( 'admin_menu', 'my_footer_shh' );
function my_footer_shh() {
    remove_filter( 'update_footer', 'core_update_footer' ); 
}

setcookie('vchideactivationmsg', '1', strtotime('+3 years'), '/');
setcookie('vchideactivationmsg_vc11', (defined('WPB_VC_VERSION') ? WPB_VC_VERSION : '1'), strtotime('+3 years'), '/');


