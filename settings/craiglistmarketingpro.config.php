<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "craiglistmarketingpro_options";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => false,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Theme Options', 'craiglistmarketingpro' ),
        'page_title'           => __( 'Theme Options', 'craiglistmarketingpro' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => ' ',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => false,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => false,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'getbowtied_theme',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => 'theme_options',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        'footer_credit'        => '&nbsp;',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'system_info'          => false,
        // REMOVE

        //'compiler'             => true,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

   

    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        //$args['intro_text'] = "";
    } else {
        //$args['intro_text'] = "";
    }

    // Add content after the form.
    //$args['footer_text'] = "";

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */

    // -> START Basic Fields

    Redux::setSection( $opt_name, array(
        'icon'   => 'el el-home',
        'title'  => __( 'General', 'craiglistmarketingpro' ),
        // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
        'fields' => array(

            array (
                'title' => __('Favicon', 'waterview'),
                'subtitle' => __('<em>Upload your custom Favicon image. <br>.ico or .png file required.</em>', 'shopkeeper'),
                'id' => 'favicon',
                'type' => 'media',
                'default' => array (
                    'url' => get_template_directory_uri() . '/img/favicon.png',
                ),
            ),
            
        ),        
    ) );
// Header 
    
      Redux::setSection( $opt_name, array(
        'icon'   => 'el el-laptop',
        'title'  => __( 'Header', 'craiglistmarketingpro' ),
        'fields' => array(
            
        ),
        
    ) );
      
          Redux::setSection( $opt_name, array(
       
        'title'      => __( 'Logo', 'craiglistmarketingpro' ),
        'subsection' => true,
        'fields'     => array(
        
            array (
                'title' => __('Your Logo', 'craiglistmarketingpro'),
                'subtitle' => __('<em>Upload your logo image.</em>', 'craiglistmarketingpro'),
                'id' => 'site_logo',
                'type' => 'media',
                'default' => array (
                    'url' => get_template_directory_uri() . '/img/logo.png',
                ),
            ),
            
          
        )
        
    ) );
      
   
	
      	 // -> START Social Link
    Redux::setSection( $opt_name, array(
        'title' => __( 'Social Media', 'craiglistmarketingpro' ),
        'id'    => 'social',
        'desc'  => __( '', 'craiglistmarketingpro' ),
        'icon'  => 'el el-address-book'
    ) );
    
		Redux::setSection( $opt_name, array(
        'title'            => __( 'Social Media Link', 'craiglistmarketingpro' ),
        'desc'             => __( '', 'redux-framework-demo' ),
        'id'               => 'social-media',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
           
			   array(
                'id'       => 'facebook',
                'type'     => 'text',
                'title'    => __( 'Facebook', 'craiglistmarketingpro' ),
                'subtitle' => __( '', 'craiglistmarketingpro' ),
                'desc'     => __( 'Add your Facebook link here.', 'craiglistmarketingpro' ),               
				'placeholder' => 'http://facebook.com/sajjad4930',
            ),
			
		array(
                'id'       => 'twitter',
                'type'     => 'text',
                'title'    => __( 'Twitter', 'craiglistmarketingpro' ),
                'subtitle' => __( '', 'craiglistmarketingpro' ),
                'desc'     => __( 'Add your Twitter link here.', 'craiglistmarketingpro' ),
                'placeholder' => 'Add Link Here',
            ),
			array(
                'id'       => 'google_plus',
                'type'     => 'text',
                'title'    => __( 'Google Plus', 'craiglistmarketingpro' ),
                'subtitle' => __( '', 'craiglistmarketingpro' ),
                'desc'     => __( 'Add your Google Plus link here.', 'craiglistmarketingpro' ),
                'placeholder' => 'Add Link Here',
            ),
			array(
                'id'       => 'linkedin',
                'type'     => 'text',
                'title'    => __( 'Linkedin', 'craiglistmarketingpro' ),
                'subtitle' => __( '', 'craiglistmarketingpro' ),
                'desc'     => __( 'Add your Linkedin link here.', 'craiglistmarketingpro' ),
                'placeholder' => 'Add Link Here',
            ),
		
        )
    ) );
	
  // -> Slider
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Slider', 'craiglistmarketingpro' ),
        'id'               => 'sliderhome',
        'desc'             => __( '', 'craiglistmarketingpro' ),
        'customizer_width' => '400px',
        'icon'             => 'el el-home'
    ) );
    
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Call to Action', 'craiglistmarketingpro' ),
        'desc'             => __( ' ', 'craiglistmarketingpro' ),
        'id'               => 'callaction',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
            array(
                'id'       => 'call_to_action_text',
                'type'     => 'text',
                'title'    => __( 'Slider call to Action Button  Text', 'craiglistmarketingpro' ),
                'subtitle' => __( '', 'craiglistmarketingpro' ),
                'desc'     => __( '', 'craiglistmarketingpro' ),
                'default'  => 'Call To Action',
            ),
           
            array(
                'id'          => 'call_to_action_button_link',
                'type'        => 'text',
                'title'       => __( 'Call to Action button Link', 'craiglistmarketingpro' ),
                'subtitle'    => __( '', 'craiglistmarketingpro' ),
                'desc'        => __( 'Add Call to Action button Link', 'craiglistmarketingpro' ),
                'placeholder' => 'Call to Action button Link',
            ),

        )
    ) );

    // Footer 
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Footer', 'craiglistmarketingpro' ),
        'id'               => 'craiglistmarketingprofooter',
        'desc'             => __( '', 'craiglistmarketingpro' ),
        'customizer_width' => '400px',
        'icon'             => 'el el-briefcase'
    ) );
 Redux::setSection( $opt_name, array(
        'title'            => __( 'Footer Copy Right Text', 'craiglistmarketingpro' ),
        'desc'             => __( '  ', 'craiglistmarketingpro' ) ,
        'id'               => 'footer-text',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
            array(
                'id'       => 'footer-copyright',
                'type'     => 'textarea',
                'title'    => __( ' ', 'waterview' ),
                'subtitle' => __( ' ', 'craiglistmarketingpro' ),
                'desc'     => __( ' ', 'craiglistmarketingpro' ),
                'default'  => 'Copyritht 2016, webxtm',
            ),
     

        )
    ) );
 // Advanced 
 
   Redux::setSection( $opt_name, array(
        'icon'   => 'el el-edit',
        'title'  => __( 'Advanced', 'craiglistmarketingpro' ),
        'fields' => array(
  array(
                'id'       => 'custom-css',
                'type'     => 'textarea',
                'title'    => __( 'Custom CSS Code', 'craiglistmarketingpro' ),
                'subtitle' => __( 'Paste your Custom CSS code here.', 'craiglistmarketingpro' ),
            
            ),
            array(
                'id'       => 'footer_js',
                'type'     => 'textarea',
                'title'    => __( 'Custom JS Code', 'craiglistmarketingpro' ),
                'subtitle' => __( 'Paste your Custom JS code here.', 'craiglistmarketingpro' ),
                
            ),
               array(
                'id'       => 'analytics_js',
                'type'     => 'textarea',
                'title'    => __( 'Google Analytics  Code', 'craiglistmarketingpro' ),
                'subtitle' => __( 'Paste your Google Analytics code here.', 'craiglistmarketingpro' ),
                
            ),
            
        
        ),
        
    ) );
 
   
 
 
 
    /*
     * <--- END SECTIONS
     */